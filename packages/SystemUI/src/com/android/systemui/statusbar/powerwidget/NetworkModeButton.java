package com.android.systemui.statusbar.powerwidget;

import com.android.systemui.R;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.provider.Settings.SettingNotFoundException;
import android.provider.Settings;
import android.util.Log;

import com.android.internal.telephony.Phone;
import android.telephony.TelephonyManager;
import java.util.ArrayList;
import java.util.List;
import com.android.internal.telephony.PhoneConstants;

public class NetworkModeButton extends PowerButton{
    private static final String TAG = "NetworkModeButton";

    private static final List<Uri> OBSERVED_URIS = new ArrayList<Uri>();
    static {
        OBSERVED_URIS.add(Settings.Global.getUriFor(Settings.Global.PREFERRED_NETWORK_MODE));
    }

    // retrieved from Phone.apk
    public static final String ACTION_NETWORK_MODE_CHANGED = "com.android.internal.telephony.NETWORK_MODE_CHANGED";
    public static final String ACTION_REQUEST_NETWORK_MODE = "com.android.internal.telephony.REQUEST_NETWORK_MODE";
    public static final String ACTION_MODIFY_NETWORK_MODE = "com.android.internal.telephony.MODIFY_NETWORK_MODE";
    public static final String EXTRA_NETWORK_MODE = "networkMode";

    private static final int NO_NETWORK_MODE_YET = -99;
    private static final int NETWORK_MODE_UNKNOWN = -100;

    private static final int CM_MODE_3G2G = 0;
    private static final int CM_MODE_3GONLY = 1;
    private static final int CM_MODE_BOTH = 2;

    private int mMode = NO_NETWORK_MODE_YET;
    private int mIntendedMode = NO_NETWORK_MODE_YET;
    private int mInternalState = STATE_INTERMEDIATE;

    public NetworkModeButton() { mType = BUTTON_NETWORKMODE; }

    @Override
    protected void updateState(Context context) {
try
{
        mState =  Settings.Global.getInt(context.getContentResolver(), Settings.Global.PREFERRED_NETWORK_MODE);

	if (mState == PhoneConstants.NT_MODE_GSM_ONLY)
		mIcon = R.drawable.stat_2g3g_off;
	else
		mIcon = R.drawable.stat_2g3g_on;
}
catch (SettingNotFoundException e)
{
}
    }

    @Override
    protected void toggleState(Context context) {

try
{
	TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);


        int mDataState = Settings.Global.getInt(context.getContentResolver(), Settings.Global.PREFERRED_NETWORK_MODE);


	if (mDataState == PhoneConstants.NT_MODE_GSM_ONLY) {
                            tm.toggle2G(false);
                        } else {
                            tm.toggle2G(true);
                        }
} catch (SettingNotFoundException ex)
{
}
    }

    @Override
    protected boolean handleLongClick(Context context) {
        // it may be better to make an Intent action for this or find the appropriate one
        // we may want to look at that option later
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.setClassName("com.android.phone", "com.android.phone.Settings");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        return true;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getExtras() != null) {
            mMode = intent.getExtras().getInt(EXTRA_NETWORK_MODE);
            //Update to actual state
            mIntendedMode = mMode;
        }

        //need to clear intermediate states
        mInternalState = STATE_ENABLED;
        mInternalState = networkModeToState(context);
    }

    @Override
    protected IntentFilter getBroadcastIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_NETWORK_MODE_CHANGED);
        return filter;
    }

    @Override
    protected List<Uri> getObservedUris() {
        return OBSERVED_URIS;
    }

    private static int get2G3G(Context context) {
        int state = 99;
        try {
            state = Settings.Global.getInt(context.getContentResolver(),
                    Settings.Global.PREFERRED_NETWORK_MODE);
        } catch (SettingNotFoundException e) {
        }
        return state;
    }

    private int networkModeToState(Context context) {
        if (mInternalState == STATE_TURNING_ON || mInternalState == STATE_TURNING_OFF) {
            return STATE_INTERMEDIATE;
        }

        switch (mMode) {
            case Phone.NT_MODE_WCDMA_PREF:
            case Phone.NT_MODE_WCDMA_ONLY:
            case Phone.NT_MODE_GSM_UMTS:
                return STATE_ENABLED;
            case Phone.NT_MODE_GSM_ONLY:
                return STATE_DISABLED;
            case Phone.NT_MODE_CDMA:
            case Phone.NT_MODE_CDMA_NO_EVDO:
            case Phone.NT_MODE_EVDO_NO_CDMA:
            case Phone.NT_MODE_GLOBAL:
                // need to check wtf is going on
                Log.d(TAG, "Unexpected network mode (" + mMode + ")");
                return STATE_DISABLED;
        }
        return STATE_INTERMEDIATE;
    }

    private static int getCurrentCMMode(Context context) {
        return /*Settings.System.getInt(context.getContentResolver(),
                Settings.System.EXPANDED_NETWORK_MODE, */
                CM_MODE_3G2G /*)*/;
    }
}
