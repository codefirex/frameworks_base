<ul id="nav">


  <li class="nav-section">
    <div class="nav-section-header">
      <a href="<?cs var:toroot ?>training/index.html">
        <span class="en">Get Started</span>
      </a>
    </div>

    <ul>
      <li class="nav-section">
        <div class="nav-section-header">
          <a href="<?cs var:toroot ?>training/basics/firstapp/index.html">
            <span class="en">Building Your First App</span>
          </a>
        </div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/basics/firstapp/creating-project.html">
            <span class="en">Creating an Android Project</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/basics/firstapp/running-app.html">
            <span class="en">Running Your Application</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/basics/firstapp/building-ui.html">
            <span class="en">Building a Simple User Interface</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/basics/firstapp/starting-activity.html">
            <span class="en">Starting Another Activity</span>
          </a>
          </li>
        </ul>
      </li>

      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/basics/activity-lifecycle/index.html">
            <span class="en">Managing the Activity Lifecycle</span>
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/basics/activity-lifecycle/starting.html">
            <span class="en">Starting an Activity</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/basics/activity-lifecycle/pausing.html">
            <span class="en">Pausing and Resuming an Activity</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/basics/activity-lifecycle/stopping.html">
            <span class="en">Stopping and Restarting an Activity</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/basics/activity-lifecycle/recreating.html">
            <span class="en">Recreating an Activity</span>
          </a>
          </li>
        </ul>
      </li>

      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/basics/supporting-devices/index.html">
            <span class="en">Supporting Different Devices</span>
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/basics/supporting-devices/languages.html">
            <span class="en">Supporting Different Languages</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/basics/supporting-devices/screens.html">
            <span class="en">Supporting Different Screens</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/basics/supporting-devices/platforms.html">
            <span class="en">Supporting Different Platform Versions</span>
          </a>
          </li>
        </ul>
      </li>

      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/basics/fragments/index.html">
            <span class="en">Building a Dynamic UI with Fragments</span>
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/basics/fragments/support-lib.html">
            <span class="en">Using the Support Library</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/basics/fragments/creating.html">
            <span class="en">Creating a Fragment</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/basics/fragments/fragment-ui.html">
            <span class="en">Building a Flexible UI</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/basics/fragments/communicating.html">
            <span class="en">Communicating with Other Fragments</span>
          </a>
          </li>
        </ul>
      </li>

      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot?>training/basics/data-storage/index.html">
            <span class="en">Saving Data</span>
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/basics/data-storage/shared-preferences.html">
            <span class="en">Saving Key-Value Sets</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/basics/data-storage/files.html">
            <span class="en">Saving Files</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/basics/data-storage/databases.html">
            <span class="en">Saving Data in SQL Databases</span>
          </a>
          </li>
        </ul>
      </li>

      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/basics/intents/index.html">
            <span class="en">Interacting with Other Apps</span>
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/basics/intents/sending.html">
            <span class="en">Sending the User to Another App</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/basics/intents/result.html">
            <span class="en">Getting a Result from the Activity</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/basics/intents/filters.html">
            <span class="en">Allowing Other Apps to Start Your Activity</span>
          </a>
          </li>
        </ul>
      </li>


    </ul>
  </li><!-- end basic training -->
  <li class="nav-section">
    <div class="nav-section-header">
      <a href="<?cs var:toroot ?>training/advanced.html">
      <span class="en">Advanced Training</span>
      </a>
    </div>
    <ul>

      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/basics/location/index.html">
            <span class="en">Making Your App Location Aware</span>
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/basics/location/locationmanager.html">
            <span class="en">Using the Location Manager</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/basics/location/currentlocation.html">
            <span class="en">Obtaining the Current Location</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/basics/location/geocoding.html">
            <span class="en">Displaying a Location Address</span>
          </a>
          </li>
        </ul>
      </li>
    </ul>
  </li><!-- end getting started -->
  
  
  

       <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/basics/network-ops/index.html">
            <span class="en">Performing Network Operations</span>
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/basics/network-ops/connecting.html">
            <span class="en">Connecting to the Network</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/basics/network-ops/managing.html">
            <span class="en">Managing Network Usage</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/basics/network-ops/xml.html">
            <span class="en">Parsing XML Data</span>
          </a>
          </li>
        </ul>
      </li>

      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/efficient-downloads/index.html">
            <span class="en">Transferring Data Without Draining the Battery</span>
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/efficient-downloads/efficient-network-access.html">
            <span class="en">Optimizing Downloads for Efficient Network Access</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/efficient-downloads/regular_updates.html">
            <span class="en">Minimizing the Effect of Regular Updates</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/efficient-downloads/redundant_redundant.html">
            <span class="en">Redundant Downloads are Redundant</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/efficient-downloads/connectivity_patterns.html">
            <span class="en">Modifying Patterns Based on the Connectivity Type</span>
          </a>
          </li>
        </ul>
      </li>

      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/cloudsync/index.html">
            <span class="en">Syncing to the Cloud</span>
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/cloudsync/backupapi.html">
            <span class="en">Using the Backup API</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/cloudsync/gcm.html">
            <span class="en">Making the Most of Google Cloud Messaging</span>
          </a>
          </li>
        </ul>
      </li>

      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/multiscreen/index.html"
          zh-CN-lang="针对多种屏幕进行设计"
          ja-lang="複数画面のデザイン"
          es-lang="Cómo diseñar aplicaciones para varias pantallas"
          >Designing for Multiple Screens</a>
        </div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/multiscreen/screensizes.html"
            zh-CN-lang="支持各种屏幕尺寸"
            ko-lang="다양한 화면 크기 지원"
            ja-lang="さまざまな画面サイズのサポート"
            es-lang="Cómo admitir varios tamaños de pantalla"
            >Designing for Multiple Screens</a>
          </li>
          <li><a href="<?cs var:toroot ?>training/multiscreen/screendensities.html"
            zh-CN-lang="支持各种屏幕密度"
            ja-lang="さまざまな画面密度のサポート"
            es-lang="Cómo admitir varias densidades de pantalla"
            >Supporting Different Screen Densities</a>
          </li>
          <li><a href="<?cs var:toroot ?>training/multiscreen/adaptui.html"
            zh-CN-lang="实施自适应用户界面流程"
            ja-lang="順応性のある UI フローの実装"
            es-lang="Cómo implementar interfaces de usuario adaptables"
            >Implementing Adaptive UI Flows</a>
          </li>
        </ul>
      </li>

      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/improving-layouts/index.html">
            <span class="en">Improving Layout Performance</span>
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/improving-layouts/optimizing-layout.html">
            <span class="en">Optimizing Layout Hierarchies</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/improving-layouts/reusing-layouts.html">
            <span class="en">Re-using Layouts with &lt;include/&gt;</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/improving-layouts/loading-ondemand.html">
            <span class="en">Loading Views On Demand</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/improving-layouts/smooth-scrolling.html">
            <span class="en">Making ListView Scrolling Smooth</span>
          </a>
          </li>
        </ul>
      </li>

        <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/animation/index.html">
            <span class="en">Adding Animations</span>
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/animation/crossfade.html">
            <span class="en">Crossfading Two Views</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/animation/screen-slide.html">
            <span class="en">Using ViewPager for Screen Slide</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/animation/cardflip.html">
            <span class="en">Displaying Card Flip Animations</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/animation/zoom.html">
            <span class="en">Zooming a View</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/animation/layout.html">
            <span class="en">Animating Layout Changes</span>
          </a>
          </li>
        </ul>
      </li>
      
      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/managing-audio/index.html">
            <span class="en">Managing Audio Playback</span>
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/managing-audio/volume-playback.html">
            <span class="en">Controlling Your App?s Volume and Playback</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/managing-audio/audio-focus.html">
            <span class="en">Managing Audio Focus</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/managing-audio/audio-output.html">
            <span class="en">Dealing with Audio Output Hardware</span>
          </a>
          </li>
        </ul>
      </li>

      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/monitoring-device-state/index.html"
          zh-CN-lang="优化电池使用时间"
          ja-lang="電池消費量の最適化"
          es-lang="Cómo optimizar la duración de la batería"
          >Optimizing Battery Life</a>
        </div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/monitoring-device-state/battery-monitoring.html"
            zh-CN-lang="监控电池电量和充电状态"
            ja-lang="電池残量と充電状態の監視"
            es-lang="Cómo controlar el nivel de batería y el estado de carga"
            >Monitoring the Battery Level and Charging State</a>
          </li>
          <li><a href="<?cs var:toroot ?>training/monitoring-device-state/docking-monitoring.html"
            zh-CN-lang="确定和监控基座对接状态和类型"
            ja-lang="ホルダーの装着状態とタイプの特定と監視"
            es-lang="Cómo determinar y controlar el tipo de conector y el estado de la conexión"
            >Determining and Monitoring the Docking State and Type</a>
          </li>
          <li><a href="<?cs var:toroot ?>training/monitoring-device-state/connectivity-monitoring.html"
            zh-CN-lang="确定和监控网络连接状态"
            ja-lang="接続状態の特定と監視"
            es-lang="Cómo determinar y controlar el estado de la conectividad"
            >Determining and Monitoring the Connectivity Status</a>
          </li>
          <li><a href="<?cs var:toroot ?>training/monitoring-device-state/manifest-receivers.html"
            zh-CN-lang="根据需要操作广播接收器"
            ja-lang="オンデマンドでのブロードキャスト レシーバ操作"
            es-lang="Cómo manipular los receptores de emisión bajo demanda"
            >Manipulating Broadcast Receivers On Demand</a>
          </li>
        </ul>
      </li>
      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/custom-views/index.html">
            <span class="en">Creating Custom Views</span>
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/custom-views/create-view.html">
            <span class="en">Creating a Custom View Class</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/custom-views/custom-drawing.html">
            <span class="en">Implementing Custom Drawing</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/custom-views/making-interactive.html">
            <span class="en">Making the View Interactive</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/custom-views/optimizing-view.html">
            <span class="en">Optimizing the View</span>
          </a>
          </li>
        </ul>
      </li>

      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/search/index.html">
            <span class="en">Adding Search Functionality</span>
          </a>
        </div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/search/setup.html">
            <span class="en">Setting up the Search Interface</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/search/search.html">
            <span class="en">Storing and Searching for Data</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/search/backward-compat.html">
            <span class="en">Remaining Backward Compatible</span>
          </a>
          </li>
        </ul>
      </li>

      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/id-auth/index.html">
            <span class="en">Remembering Users</span>
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/id-auth/identify.html">
            <span class="en">Remembering Your User</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/id-auth/authenticate.html">
            <span class="en">Authenticating to OAuth2 Services</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/id-auth/custom_auth.html">
            <span class="en">Creating a Custom Account Type</span>
          </a>
          </li>
        </ul>
      </li>

      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/sharing/index.html">
            <span class="en">Sharing Content</span>
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/sharing/send.html">
            <span class="en">Sending Content to Other Apps</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/sharing/receive.html">
            <span class="en">Receiving Content from Other Apps</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/sharing/shareaction.html">
            <span class="en">Adding an Easy Share Action</span>
          </a>
          </li>
        </ul>
      </li>

      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/camera/index.html">
            <span class="en">Capturing Photos</span>
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/camera/photobasics.html">
            <span class="en">Taking Photos Simply</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/camera/videobasics.html">
            <span class="en">Recording Videos Simply</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/camera/cameradirect.html">
            <span class="en">Controlling the Camera</span>
          </a>
          </li>
        </ul>
      </li>

      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/multiple-apks/index.html">
            <span class="en">Maintaining Multiple APKs</span>
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/multiple-apks/api.html">
            <span class="en">Creating Multiple APKs for Different API Levels</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/multiple-apks/screensize.html">
            <span class="en">Creating Multiple APKs for Different Screen Sizes</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/multiple-apks/texture.html">
            <span class="en">Creating Multiple APKs for Different GL Textures</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/multiple-apks/multiple.html">
            <span class="en">Creating Multiple APKs with 2+ Dimensions</span>
          </a>
          </li>
        </ul>
      </li>

      <li class="nav-section">
          <div class="nav-section-header">
              <a href="<?cs var:toroot ?>training/notify-user/index.html"
                 description=
                 "How to display messages called notifications outside of
                 your application's UI."
               >Notifying the User</a>
          </div>
          <ul>
              <li>
                  <a href="<?cs var:toroot ?>training/notify-user/build-notification.html">
                  Building a Notification
                  </a>
              </li>
              <li>
                  <a href="<?cs var:toroot ?>training/notify-user/navigation.html">
                  Preserving Navigation when Starting an Activity
                  </a>
              </li>
              <li>
                  <a href="<?cs var:toroot ?>training/notify-user/managing.html">
                  Updating Notifications
                  </a>
              </li>
              <li>
                  <a href="<?cs var:toroot ?>training/notify-user/expanded.html">
                  Using Big View Styles
                  </a>
              </li>
              <li>
                  <a href="<?cs var:toroot ?>training/notify-user/display-progress.html">
                  Displaying Progress in a Notification
                  </a>
              </li>
          </ul>
      </li>

      <li class="nav-section">
        <div class="nav-section-header">
          <a href="<?cs var:toroot ?>training/search/index.html"
             description=
             "How to properly add a search interface to your app and create a searchable database."
            >Adding Search Functionality</a>
        </div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/search/setup.html">
            Setting up the Search Interface
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/search/search.html">
            Storing and Searching for Data
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/search/backward-compat.html">
            Remaining Backward Compatible
          </a>
          </li>
        </ul>
      </li>
      
      
      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/backward-compatible-ui/index.html">
            <span class="en">Creating Backward-Compatible UIs</span>
          </a></div>
        <ul>
          <li><a href="/training/multiscreen/screensizes.html"
            zh-CN-lang="支持各种屏幕尺寸"
            ko-lang="다양한 화면 크기 지원"
            ja-lang="さまざまな画面サイズのサポート"
            es-lang="Cómo admitir varios tamaños de pantalla"
            >Supporting Different Screen Sizes</a>
          </li>
          <li><a href="/training/multiscreen/screendensities.html"
            zh-CN-lang="支持各种屏幕密度"
            ja-lang="さまざまな画面密度のサポート"
            es-lang="Cómo admitir varias densidades de pantalla"
            >Supporting Different Screen Densities</a>
          </li>
          <li><a href="/training/multiscreen/adaptui.html"
            zh-CN-lang="实施自适应用户界面流程"
            ja-lang="順応性のある UI フローの実装"
            es-lang="Cómo implementar interfaces de usuario adaptables"
            >Implementing Adaptive UI Flows</a>
          </li>
        </ul>
      </li>

      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/enterprise/index.html">
            <span class="en">Developing for Enterprise</span>
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/enterprise/device-management-policy.html">
            <span class="en">Enhancing Security with Device Management Policies</span>
          </a>
          </li>
        </ul>
      </li>
      
      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/design-navigation/index.html">
            <span class="en">Designing Effective Navigation</span>
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/design-navigation/screen-planning.html">
            <span class="en">Planning Screens and Their Relationships</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/design-navigation/multiple-sizes.html">
            <span class="en">Planning for Multiple Touchscreen Sizes</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/design-navigation/descendant-lateral.html">
            <span class="en">Providing Descendant and Lateral Navigation</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/design-navigation/ancestral-temporal.html">
            <span class="en">Providing Ancestral and Temporal Navigation</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/design-navigation/wireframing.html">
            <span class="en">Putting it All Together: Wireframing the Example App</span>
          </a>
          </li>
        </ul>
      </li>

      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/implementing-navigation/index.html">
            <span class="en">Implementing Effective Navigation</span>
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/implementing-navigation/lateral.html">
            <span class="en">Implementing Lateral Navigation</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/implementing-navigation/ancestral.html">
            <span class="en">Implementing Ancestral Navigation</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/implementing-navigation/temporal.html">
            <span class="en">Implementing Temporal Navigation</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/implementing-navigation/descendant.html">
            <span class="en">Implementing Descendant Navigation</span>
          </a>
          </li>
        </ul>
      </li>

      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/tv/index.html">
           <span class="en">Designing for TV</span>
           </a>
        </div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/tv/optimizing-layouts-tv.html">
            <span class="en">Optimizing Layouts for TV</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/tv/optimizing-navigation-tv.html">
            <span class="en">Optimizing Navigation for TV</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/tv/unsupported-features-tv.html">
            <span class="en">Handling Features Not Supported on TV</span>
          </a>
          </li>
        </ul>
      </li>
      
    </ul>
  </li>
  <!-- End best UX and UI -->
  

  <li class="nav-section">
    <div class="nav-section-header">
      <a href="<?cs var:toroot ?>training/best-user-input.html">
      <span class="small">Best Practices for</span><br/>
              User Input
      </a>
    </div>
    <ul>
         
      <li class="nav-section">
        <div class="nav-section-header">
          <a href="<?cs var:toroot ?>training/gestures/index.html"
             description=
             "How to write apps that allow users to interact with the touch screen via touch gestures."
            >Using Touch Gestures</a>
        </div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/gestures/detector.html">
            Detecting Common Gestures
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/gestures/movement.html">
            Tracking Movement
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/gestures/scroll.html">
            Animating a Scroll Gesture
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/gestures/multi.html">
            Handling Multi-Touch Gestures
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/gestures/scale.html">
            Dragging and Scaling
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/gestures/viewgroup.html">
            Managing Touch Events in a ViewGroup
          </a>
          </li>
        </ul>
      </li>
      
      <li class="nav-section">
        <div class="nav-section-header">
          <a href="<?cs var:toroot ?>training/keyboard-input/index.html"
             description=
             "How to specify the appearance and behaviors of soft input methods (such
             as on-screen keyboards) and how to optimize the experience with
             hardware keyboards."
            >Handling Keyboard Input</a>
        </div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/keyboard-input/style.html">
            Specifying the Input Method Type
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/keyboard-input/visibility.html">
            Handling Input Method Visibility
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/keyboard-input/navigation.html">
            Supporting Keyboard Navigation
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/keyboard-input/commands.html">
            Handling Keyboard Actions
          </a>
          </li>
        </ul>
      </li>
    </ul>
  </li> <!-- end of User Input -->    

      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/displaying-bitmaps/index.html">
            <span class="en">Displaying Bitmaps Efficiently</span>
          </a>
        </div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/displaying-bitmaps/load-bitmap.html">
            <span class="en">Loading Large Bitmaps Efficiently</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/displaying-bitmaps/process-bitmap.html">
            <span class="en">Processing Bitmaps Off the UI Thread</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/displaying-bitmaps/cache-bitmap.html">
            <span class="en">Caching Bitmaps</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/displaying-bitmaps/display-bitmap.html">
            <span class="en">Displaying Bitmaps in Your UI</span>
          </a></li>
        </ul>
      </li>

      <li class="nav-section">
        <div class="nav-section-header">
          <a href="<?cs var:toroot ?>training/run-background-service/index.html"
             description=
             "How to improve UI performance and responsiveness by sending work to a
             Service running in the background"
            >Running in a Background Service</a>
        </div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/run-background-service/create-service.html">
            Creating a Background Service
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/run-background-service/send-request.html">
            Sending Work Requests to the Background Service
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/run-background-service/report-status.html">
            Reporting Work Status
          </a>
          </li>
        </ul>
      </li>

      <li class="nav-section">
        <div class="nav-section-header">
          <a href="<?cs var:toroot ?>training/load-data-background/index.html"
             description="How to use CursorLoader to query data without
             affecting UI responsiveness."
            >Loading Data in the Background</a>
        </div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/load-data-background/setup-loader.html">
            Running a Query with a CursorLoader</a>
          </li>
          <li><a href="<?cs var:toroot ?>training/load-data-background/handle-results.html">
            Handling the Results</a>
          </li>
        </ul>
      </li>
      
      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/accessibility/index.html">
            <span class="en">Implementing Accessibility</span>
          </a></div>
        <ul>
          <li><a href="/training/monitoring-device-state/battery-monitoring.html"
            zh-CN-lang="监控电池电量和充电状态"
            ja-lang="電池残量と充電状態の監視"
            es-lang="Cómo controlar el nivel de batería y el estado de carga"
            >Monitoring the Battery Level and Charging State</a>
          </li>
          <li><a href="/training/monitoring-device-state/docking-monitoring.html"
            zh-CN-lang="确定和监控基座对接状态和类型"
            ja-lang="ホルダーの装着状態とタイプの特定と監視"
            es-lang="Cómo determinar y controlar el tipo de conector y el estado de la conexión"
            >Determining and Monitoring the Docking State and Type</a>
          </li>
          <li><a href="/training/monitoring-device-state/connectivity-monitoring.html"
            zh-CN-lang="确定和监控网络连接状态"
            ja-lang="接続状態の特定と監視"
            es-lang="Cómo determinar y controlar el estado de la conectividad"
            >Determining and Monitoring the Connectivity Status</a>
          </li>
          <li><a href="/training/monitoring-device-state/manifest-receivers.html"
            zh-CN-lang="根据需要操作广播接收器"
            ja-lang="オンデマンドでのブロードキャスト レシーバ操作"
            es-lang="Cómo manipular los receptores de emisión bajo demanda"
            >Manipulating Broadcast Receivers On Demand</a>
          </li>
        </ul>
      </li>
      <li class="nav-section">
        <div class="nav-section-header">
          <a href="<?cs var:toroot ?>training/multiple-threads/index.html"
             description=
             "How to improve the performance and scalability of long-running operations by
              dispatching work to multiple threads.">
             Sending Operations to Multiple Threads</a>
        </div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/multiple-threads/define-runnable.html">
            Specifying the Code to Run on a Thread
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/multiple-threads/create-threadpool.html">
            Creating a Manager for Multiple Threads
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/multiple-threads/run-code.html">
            Running Code on a Thread Pool Thread
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/multiple-threads/communicate-ui.html">
            Communicating with the UI Thread
          </a>
          </li>
        </ul>
      </li>
      
      <li>
        <a href="<?cs var:toroot ?>training/articles/perf-anr.html"
           description=
           "How to keep your app responsive to user interaction so the UI does not lock-up and
           display an &quot;Application Not Responding&quot; dialog."
          >Keeping Your App Responsive</a>
      </li>
      
      <li>
        <a href="<?cs var:toroot ?>training/articles/perf-jni.html"
           description=
           "How to efficiently use the Java Native Interface with the Android NDK."
          >JNI Tips</a>
      </li>
      <li>
        <a href="<?cs var:toroot ?>training/articles/smp.html"
           description=
           "Tips for coding Android apps on symmetric multiprocessor systems."
          >SMP Primer for Android</a>
      </li>
    </ul>
  </li> <!-- end of Performance -->
      
      


      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot
?>training/graphics/opengl/index.html">
            <span class="en">Displaying Graphics with OpenGL ES</span>
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/graphics/opengl/environment.html">
            <span class="en">Building an OpenGL ES Environment</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/graphics/opengl/shapes.html">
            <span class="en">Defining Shapes</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/graphics/opengl/draw.html">
            <span class="en">Drawing Shapes</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/graphics/opengl/projection.html">
            <span class="en">Applying Projection and Camera Views</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/graphics/opengl/motion.html">
            <span class="en">Adding Motion</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/graphics/opengl/touch.html">
            <span class="en">Responding to Touch Events</span>
          </a>
          </li>
        </ul>
      </li>


  <li class="nav-section">
    <div class="nav-section-header">
      <a href="<?cs var:toroot ?>training/distribute.html">
      <span class="small">Using Google Play to</span><br/>
              Distribute &amp; Monetize
      </a>
    </div>
    <ul>
      <li class="nav-section">
      <div class="nav-section-header"><a href="<?cs var:toroot ?>training/in-app-billing/index.html"
         description="How to sell in-app products from your application using In-app Billing.">
            Selling In-app Products
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/in-app-billing/preparing-iab-app.html">
            <span class="en">Preparing Your App</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/in-app-billing/list-iab-products.html">
            <span class="en">Establishing Products for Sale</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/in-app-billing/purchase-iab-products.html">
            <span class="en">Purchasing Products</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/in-app-billing/test-iab-app.html">
            <span class="en">Testing Your App</span>
          </a>
          </li>
        </ul>
      </li>
      
      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/connect-devices-wirelessly/index.html">
            <span class="en">Connecting Devices Wirelessly</span>
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/connect-devices-wirelessly/nsd.html">
            <span class="en">Using Network Service Discovery</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/connect-devices-wirelessly/wifi-direct.html">
            <span class="en">Connecting with Wi-Fi Direct</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/connect-devices-wirelessly/nsd-wifi-direct.html">
            <span class="en">Using Wi-Fi Direct for Service Discovery</span>
          </a>
          </li>
        </ul>
      </li>
      <li class="nav-section">
        <div class="nav-section-header"><a href="<?cs var:toroot ?>training/load-data-background/index.html">
            <span class="en">Loading Data in the Background</span>
          </a></div>
        <ul>
          <li><a href="<?cs var:toroot ?>training/load-data-background/setup-loader.html">
            <span class="en">Setting Up the Loader</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/load-data-background/define-launch-query.html">
            <span class="en">Defining and Launching the Query</span>
          </a>
          </li>
          <li><a href="<?cs var:toroot ?>training/load-data-background/handle-results.html">
            <span class="en">Handling the Results</span>
          </a>
          </li>
        </ul>
      </li>

    </ul>
  </li>
</ul><!-- nav -->
<script type="text/javascript">
<!--
    buildToggleLists();
    changeNavLang(getLangPref());
//-->
</script>
